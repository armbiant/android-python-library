import pytest

from burger_maker import main

def test_menu_should_be_as_expected():
    response = main.gime_the_menu()
    expected = ["cheeseburger", "bacon", "farmer", "fish", "veggie", "frenchie"]

    assert response == expected


def test_cheeseburger_should_be_as_expected():
    response = main.gime_the_ingredients("cheeseburger")
    expected = ["bun", "red onion", "tomato", "ketchup", "salad", "cheddar", "steak"]

    assert response == expected


def test_nonexistingburger_should_fail():
    with pytest.raises(KeyError):
        main.gime_the_ingredients("nosuchrecipe")
