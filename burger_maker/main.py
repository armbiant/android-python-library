burger_recipes = {
    "cheeseburger": [
        "bun",
        "red onion",
        "tomato",
        "ketchup",
        "salad",
        "cheddar",
        "steak",
    ],
    "bacon": ["bun", "bacon", "garlic sauce", "brie", "steak", "tomato"],
    "farmer": ["bun", "red onion", "bbq sauce", "stilton", "chicken", "tomato"],
    "fish": ["bun", "tartare sauce", "cucumber", "old gouda", "fish fillet"],
    "veggie": [
        "bun",
        "goat cheese",
        "tomato confit",
        "red onion",
        "super-secret veggie steak",
    ],
    "frenchie": ["baguette", "butter", "brie", "smoked ham"],
}


def gime_the_menu() -> list[str]:
    return list(burger_recipes.keys())


def gime_the_ingredients(recipe: str) -> list[str]:
    ingredients = burger_recipes.get(recipe)
    if ingredients is None:
        raise KeyError(f"No such recipe: {recipe}")
    return ingredients
